## Laravel Page Blocks [Russian Documentation](https://gitlab.com/laravel-libs/laravel-page-blocks/blob/master/READMERU.md)

We create this package for dynamic create web pages, with different and changeable block.

### Install package
For download package:
```bash
composer require pilyavskiy/laravel-page-blocks
```

**This is only required if you are using Laravel 5.4!**
For include into project, please, add PBServiceProvider in your `config/app.php` file:
```php
Pilyavskiy\PB\PBServiceProvider::class
```

Then run migration for create base page blocks structure:
```bash
php artisan migrate
```
For you will created tables: pages and page_blocks

### Page structure:
- id - auto increment
- page - string, unique page name
- route - string, unique page route, by value from this field, you will have access to you page.*
- title - string, your page title
- metaTitle - string, uses for html tag `<title>`
- metaDescription - text, uses for html tag `<description>`
- metaKeywords - text, uses for html tag `<keywords>`
- isModified - boolean, flag for modified possibility 
- isDeletable - boolean, flag for delete possibility 

**Important: for index page use `/` as route**

### Create new page blocks
After include package, you will be available command for create new page block model:
```bash
php artisan make:pbmodel PageBlockExample
```

For create create new page block model with migration and view, please add flag `-b`
```bash
php artisan make:pbmodel PageBlockExample -b
```

### Routes
Page blocks routes always are the last routes registered by your application.

If you need customize some pages, please registered new route for change default package route.


### Publish
For publish views and migrations from package, please run
```bash
 php artisan vendor:publish
``` 
And choose `Provider: Pilyavskiy\PB\PBServiceProvider`.

File `resources/views/vendor/laravel-page-blocks/default.blade.php` uses for show page with page blocks, you can edit it for your needs.

File `resources/views/vendor/laravel-page-blocks/content.blade.php`, file for show page blocks.
You can include this file `laravel-page-blocks::content`, inside your own views, but be sure that you have `$page`.

