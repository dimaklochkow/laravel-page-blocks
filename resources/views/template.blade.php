<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title>{{ $page->metaTitle ?? 'Page title' }}</title>
    @if (!empty($page->metaDescription))
        <meta name="description" content="{{ $page->metaDescription ?? 'Page description'}}">
    @endif
    @if (!empty($page->metaKeywords))
        <meta name="keywords" content="{{ $page->metaKeywords ?? 'Page meta keywords'}}">
    @endif
</head>
<body>

@yield('content')

</body>
</html>

