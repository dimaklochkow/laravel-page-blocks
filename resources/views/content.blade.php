@foreach($page->blocks as $block)
    @include($block->blockable->getView(), [
        $block->blockable->getVariable() => $block->blockable,
        'display' => $block->display
    ])
@endforeach
