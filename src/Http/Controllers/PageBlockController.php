<?php

namespace Pilyavskiy\PB\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pilyavskiy\PB\Repository\PageRepository;
use Pilyavskiy\PB\Model\PageBlock;

class PageBlockController extends Controller
{
    public function show(PageRepository $pageRepository, string $slug = null)
    {
        $excludePages = config('laravel-page-blocks.exclude') ?? [];
        $page = $pageRepository->read($slug);

        if (empty($page) || in_array($slug, $excludePages)) {
            abort(404);
        }

        return view('laravel-page-blocks::default', [
            'page' => $page
        ]);
    }

    public function changeVisibility(Request $request)
    {
        $blockId = $request->get('blockId');
        $display = $request->get('display');

        PageBlock::where('id', $blockId)->update(['display' => $display]);

        return response('Block visibility successfully changed!');
    }
}
