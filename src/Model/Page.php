<?php

namespace Pilyavskiy\PB\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $fillable = [
        'page',
        'route',
        'title',
        'metaTitle',
        'metaDescription',
        'metaKeywords',
        'isActive',
        'isModified',
        'isDeletable',
    ];

    public function blocks()
    {
        return $this->hasMany('Pilyavskiy\PB\Model\PageBlock')->orderBy('sorting', 'ASC');
    }

    public static function getPage(string $slug): ?self
    {
        $page = static::where('page', $slug)->where('isActive', true)->first();

        return $page ?? null;
    }

    public static function getPageByRoute(string $route): ?self
    {
        $page = static::where('route', $route)->where('isActive', true)->first();

        return $page ?? null;
    }
}
