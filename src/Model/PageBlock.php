<?php

namespace Pilyavskiy\PB\Model;

use Illuminate\Database\Eloquent\Model;

class PageBlock extends Model
{
    protected $fillable = ['page_id', 'blockable_type', 'blockable_id', 'sorting', 'display'];

    public function blockable()
    {
        return $this->morphTo();
    }
}
