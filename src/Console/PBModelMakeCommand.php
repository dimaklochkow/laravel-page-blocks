<?php

namespace Pilyavskiy\PB\Console;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Foundation\Console\ModelMakeCommand;

class PBModelMakeCommand extends ModelMakeCommand
{
    const VIEW_DIR = 'blocks';
    const ADMIN_VIEW_DIR = 'admin';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:pbmodel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Page block eloquent model class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Model';


    public function handle()
    {
        if (parent::handle() === false && !$this->option('force')) {
            return false;
        }
        $needAdmin = config('laravel-page-blocks.admin.active') ?? false;

        if ($this->option('block')) {
            $this->createView();
            $this->createMigration();
            if ($needAdmin) {
                $this->createAdminView();
            }
        }

        if ($this->option('view')) {
            $this->createView();
            if ($needAdmin) {
                $this->createAdminView();
            }
        }

    }

    protected function createView()
    {
        if (!file_exists(resource_path('views/' . static::VIEW_DIR))) {
            @mkdir(resource_path('views/' . static::VIEW_DIR), 0777, true);
        }

        $viewPath = resource_path('views/' . static::VIEW_DIR . '/' . $this->getNameByPath($this->argument('name')) . '.blade.php');
        if (!file_exists($viewPath)) {
            $file = fopen($viewPath, 'w');
            fwrite($file, "{{-- Please, use \"{$this->getVarByPath($this->argument('name'))}\" variable! --}}
@if(\$display ?? true)
{{-- TODO Put page block here --}}
@endif");
            fclose($file);
        }

        $this->line("<info>Created View:</info> {$viewPath}");
    }

    protected function createAdminView()
    {
        if (!file_exists(resource_path($this->getAdminViewDir()))) {
            @mkdir(resource_path($this->getAdminViewDir()), 0777, true);
        }

        $viewPath = resource_path($this->getAdminViewDir() . '/' . $this->getNameByPath($this->argument('name')) . '.blade.php');
        if (!file_exists($viewPath)) {
            $file = fopen($viewPath, 'w');
            fwrite($file, "{{-- Please, use \"{$this->getVarByPath($this->argument('name'))}\" variable! --}}");
            fclose($file);
        }

        $this->line("<info>Created Admin View:</info> {$viewPath}");
    }

    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        $stub = $this->replaceViewVariable($stub, $name)
            ->replaceView($stub, $name)
            ->replaceNamespace($stub, $name)
            ->replaceClass($stub, $name);

        $needAdmin = config('laravel-page-blocks.admin.active') ?? false;
        if ($needAdmin) {
            $this->replaceAdminViewVariable($stub, $name)
                ->replaceAdminView($stub, $name);
        }

        return $stub;
    }

    protected function replaceNamespace(&$stub, $name)
    {
        parent::replaceNamespace($stub, $name);

        $pbModelNamespace = config('laravel-page-blocks.pbmodel.namespace') ?? '';
        if ($pbModelNamespace) {
            $stub = str_replace('namespace App;', "namespace {$pbModelNamespace};", $stub);
        }

        return $this;
    }

    protected function getPath($name)
    {
        $pbModelNamespace = config('laravel-page-blocks.pbmodel.namespace') ?? '';

        $name = Str::replaceFirst($this->rootNamespace(), '', $name);
        if (!empty($pbModelNamespace)) {
            $dirWithOutNamespace = Str::replaceFirst($this->rootNamespace(), '', $pbModelNamespace);
            $dirWithOutNamespace = str_replace('\\', DIRECTORY_SEPARATOR, $dirWithOutNamespace);
            $modelDir = $this->laravel['path'] . DIRECTORY_SEPARATOR . $dirWithOutNamespace;
            @mkdir($modelDir, 0777, true);

            $name = $dirWithOutNamespace . DIRECTORY_SEPARATOR . $name;
        }
        return $this->laravel['path'] . '/' . str_replace('\\', '/', $name) . '.php';
    }

    protected function replaceView(&$stub, $name)
    {
        $view = static::VIEW_DIR . '.' . $this->getNameByPath($name);
        $stub = str_replace('DummyView', $view, $stub);

        return $this;
    }

    protected function replaceAdminView(&$stub, $name)
    {
        $view = static::VIEW_DIR . '.' . static::ADMIN_VIEW_DIR . '.' . $this->getNameByPath($name);
        $stub = str_replace('DummyAdminView', $view, $stub);

        return $this;
    }

    protected function replaceViewVariable(&$stub, $name)
    {
        $variable = $this->getVarByPath($name);
        $stub = str_replace('DummyViewVariable', $variable, $stub);

        return $this;
    }

    protected function replaceAdminViewVariable(&$stub, $name)
    {
        $variable = $this->getVarByPath($name);
        $stub = str_replace('DummyAdminViewVariable', $variable, $stub);

        return $this;
    }

    private function getNameByPath($path): string
    {
        $path = str_replace($this->getNamespace($path) . '\\', '', $path);
        $path = str_replace('\\', '', $path);
        $name = Str::kebab($path);

        return $name;
    }

    private function getVarByPath($path): string
    {
        $path = str_replace($this->getNamespace($path) . '\\', '', $path);
        $path = lcfirst($path);
        $name = Str::camel($path) . 'Variable';

        return $name;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }

    private function getAdminViewDir(): string
    {
        return 'views' . DIRECTORY_SEPARATOR . static::VIEW_DIR . DIRECTORY_SEPARATOR . static::ADMIN_VIEW_DIR;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $needAdmin = config('laravel-page-blocks.admin.active') ?? false;
        if ($needAdmin) {
            return __DIR__ . '/stubs/pbmodel-admin.stub';
        }
        return __DIR__ . '/stubs/pbmodel.stub';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['block', 'b', InputOption::VALUE_NONE, 'Generate a migration, view, and resource controller for the page block model'],

            ['all', 'a', InputOption::VALUE_NONE, 'Generate a migration, factory, and resource controller for the page block model']
            ,
            ['view', 'w', InputOption::VALUE_NONE, 'Generate a view for the page block model'],

            ['controller', 'c', InputOption::VALUE_NONE, 'Create a new controller for the page block model'],

            ['factory', 'f', InputOption::VALUE_NONE, 'Create a new factory for the model'],

            ['force', null, InputOption::VALUE_NONE, 'Create the class even if the model already exists'],

            ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the page block model'],

            ['pivot', 'p', InputOption::VALUE_NONE, 'Indicates if the generated model should be a custom intermediate table model'],

            ['resource', 'r', InputOption::VALUE_NONE, 'Indicates if the generated controller should be a resource controller'],

            ['seed', 's', InputOption::VALUE_NONE, 'Create a new seeder file for the model'],

            ['api', null, InputOption::VALUE_NONE, 'Indicates if the generated controller should be an API controller'],

            ['policy', null, InputOption::VALUE_NONE, 'Indicates if the generated model should be a custom intermediate table model'],
        ];
    }
}
