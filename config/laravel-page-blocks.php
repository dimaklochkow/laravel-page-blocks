<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Page Block Model
    |--------------------------------------------------------------------------
    |
    | Settings for page block models
    |
    */

    'pbmodel' => [
        'namespace' => 'App\Models\PageBlocks'
    ],


    /*
    |--------------------------------------------------------------------------
    | Page Block Model Admin Section
    |--------------------------------------------------------------------------
    |
    | Settings for page block models
    |
    */

    'admin' => [
        'active' => true
    ],


    /*
    |--------------------------------------------------------------------------
    | Page Block Model pages slug section
    |--------------------------------------------------------------------------
    |
    | Slug name for pages for use in custom app routes
    | It is 'route' in pages table
    */

    'pages' => [
        //'examplePageName' => 'examplePageRoute',
    ],

    // List of routes what you want exclude from default route show
    'exclude' => [
        //'excludeExampleRoute',\
    ]
];
